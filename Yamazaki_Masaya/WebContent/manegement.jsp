<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<script type="text/javascript">
		function check() {
			if (window.confirm('本当に変更しますか？')) {
				return true;
			} else {
				window.alert('キャンセルされました');
				return false;
			}
		}
	</script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<div class="header">
				<a href="./">ホーム</a>
				<a href="signup">ユーザー新規登録</a><br/>
        	</div>



            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

			<c:if test="${ not empty userBrancbDepartments }">

				<c:forEach items="${userBrancbDepartments}" var="userBrancbDepartment">
	                <div class="manegement">
						<form action="manegement" method="post"><br />
							<p class="label">アカウント</p>
							<span class="account"><c:out value="${userBrancbDepartment.account}" /></span>

							<p class="label">ユーザー名</p>
							<span class="userName"><c:out value="${userBrancbDepartment.userName}" /></span>


							<p class="label">支店名</p>
							<span class="branchName"><c:out value="${userBrancbDepartment.branchName}" /></span>
							<p class="label">部署名</p>
							<span class="departmentName"><c:out value="${userBrancbDepartment.departmentName}" /></span>
	            		</form>
	            		<div class="submit">
		            		<form action="setting">
								<input type="hidden" name="userId" value="${userBrancbDepartment.userId}">
		            			<input type="submit" value="編集" />
		            		</form>
	            		</div>


						<p class="label">稼働状況</p>
						<c:choose>
							<c:when test="${userBrancbDepartment.isStopped == '0'}">
								<span class="isStopped"><c:out value="稼働" /></span><br />
							</c:when>
							<c:when test="${userBrancbDepartment.isStopped == '1'}">
								<span class="isStopped"><c:out value="停止" /></span><br />
							</c:when>
						</c:choose>

						<div class="submit">
		            		<form action="status" method="post" onSubmit="return check()">
		            			<c:if test="${ loginUser.id!=userBrancbDepartment.userId }">
		            				<input type="hidden" name="userId" value="${userBrancbDepartment.userId}">
		            				<c:choose>
										<c:when test="${userBrancbDepartment.isStopped == '0'}">
											<input type="submit" name="stop" value="停止" /><br />
										</c:when>
										<c:when test="${userBrancbDepartment.isStopped == '1'}">
											<input type="submit" name="resurrect" value="復活" /><br />
										</c:when>
									</c:choose>
								</c:if>
	            			</form>
            			</div>
                	</div>
                </c:forEach>
            </c:if>
            <div class="copyright"> Copyright(c)Yamazaki Masaya</div>
        </div>
    </body>
</html>