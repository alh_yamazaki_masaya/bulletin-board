<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="header">
				<a href="./manegement">ユーザー管理</a><br/>
			</div>

			<div class="setting">
				<form action="setting" method="post"><br />


	                <label for="account">アカウント</label>
	                <input name="account" value="<c:out value="${user.account}"/>" /><br />

	                <label for="password">パスワード</label>
	                <input name="password" type="password" id="password" /><br />

	                <label for="confirmationPassword">確認用パスワード</label>
	                <input name="confirmationPassword" type="password" id="confirmationPassword" /><br />

	                <label for="name">名前</label>
	                <input name="name" id="name" value="<c:out value="${user.name}"/>"/><br />

	                <label for="branchId">支社</label>
	                <select name="branchId">
						<c:forEach items="${branches}" var="branch">

							<c:if test="${ user.id == loginUser.id}">
								<c:if test="${user.branchId == branch.id}">
									<option value="<c:out value="${branch.id}"/>"><c:out value="${branch.name}"/></option>
								</c:if>
							</c:if>
							<c:if test="${ user.id != loginUser.id}">
								<c:if test="${user.branchId == branch.id}">
									<option value="<c:out value="${branch.id}"/>" selected><c:out value="${branch.name}"/></option>
								</c:if>
								<c:if test="${user.branchId != branch.id}">
									<option value="<c:out value="${branch.id}"/>"><c:out value="${branch.name}"/></option>
								</c:if>
							</c:if>
						</c:forEach>

					</select><br />
	                <label for="departmentId">部署</label>
	                <select name="departmentId">
						<c:forEach items="${departments}" var="department">
							<c:if test="${ user.id == loginUser.id}">
								<c:if test="${user.departmentId == department.id}">
									<option value="<c:out value="${department.id}"/>"><c:out value="${department.name}" /></option>
								</c:if>
							</c:if>
							<c:if test="${ user.id != loginUser.id}">
								<c:if test="${user.departmentId == department.id}">
									<option value="<c:out value="${department.id}"/>" selected><c:out value="${department.name}" /></option>
								</c:if>
								<c:if test="${user.departmentId != department.id}">
									<option value="<c:out value="${department.id}"/>"><c:out value="${department.name}" /></option>
								</c:if>
							</c:if>
						</c:forEach>
					</select><br />
					<input type="hidden" name="userId" value="${user.id}" />

					<div class="submit">
						<input type="submit" value="更新" /> <br />
					</div>


	            </form>
			</div>
            <div class="copyright"> Copyright(c)Yamazaki Masaya</div>
        </div>
    </body>
</html>