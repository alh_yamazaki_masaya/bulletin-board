<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

			<div class="header">
				<a href="./">ホーム</a>
			</div>

			<div class="post-message">

				<span>メッセージ投稿</span>
				<form action="message" method="post"><br />

	                <label for="title">件名</label>
	                <input name="title" value="${message.title}" id="title"/><br />

	                <label for="category">カテゴリ</label>
	                <input name="category" value="${message.category}" /><br />

	                <label for="text">投稿内容</label>
	                <textarea name="text" cols="35" rows="5" id="text"><c:out value="${message.text}" /></textarea><br />

					<div class="submit">
						<input type="submit" value="投稿" /> <br />
					</div>


         	    </form>
			</div>

            <div class="copyright"> Copyright(c)Yamazaki Masaya</div>
        </div>
    </body>
</html>