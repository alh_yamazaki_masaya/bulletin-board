<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script type="text/javascript">
	function check() {
		if (window.confirm('本当に削除しますか？')) {
			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="message">新規投稿</a>
			</c:if>
			<c:if test="${loginUser.branchId=='1'and loginUser.departmentId=='1' }">
				<a href="./manegement">ユーザー編集</a>
			</c:if>

			<c:if test="${ not empty loginUser }">
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<div class="search-message">
			<span>掲示板検索フォーム</span>
			<form action="./" class="search"><br />
				<div class="form">日付<br>
					<input type="date" name="dateFrom" id="dateFrom" value="<c:if test="${not empty dateFrom}"><c:out value="${dateFrom }" /></c:if>"/>
					～
					<input type="date" name="dateTo" id="dateTo" value="<c:if test="${not empty dateTo}"><c:out value="${dateTo }" /></c:if>"/><br />
				</div>

				<div class="form">カテゴリ<br>
					<input name="category" id="category" value="<c:if test="${not empty category}"><c:out value="${category }" /></c:if>"/>
				</div>
				<div class="submit">
					<input type="submit" name="search" value="絞込み">
				</div>

			</form>
		</div>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="show-message">
						<p class="label">投稿者名</p>
						<div class="account-name">
							<span class="name"><c:out value="${message.name}" /></span><br />
						</div>

						<p class="label">件名</p>
						<div class="account-title">
							<span class="title"><c:out value="${message.title}" /></span><br />
						</div>

						<div class="account-category">
							<p class="label">カテゴリ</p>
							<span class="category"><c:out value="${message.category}" /></span><br />
						</div>


						<p class="label">メッセージ</p>
						<div class="text">
							<span class="text"><c:forEach var="text" items="${fn:split(message.text,'
')}" ><c:out value="${text}" /><br></c:forEach></span><br/>
						</div>

						<div class="date">
							<fmt:formatDate value="${message.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
							<br />
						</div>

						<c:if test="${ loginUser.id==message.userId }">
							<form action="deleteMessage" method="post" onSubmit="return check()">
								<div class="deleteButton">
									<input type="hidden" name="messageId" id="messageId" value="${message.messageId}">
									<input type="submit" value="削除">
								</div>
							</form>
						</c:if>
					</div>


					<div class="showComment">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${ comment.messageId == message.messageId }">
								<p class="label">コメント投稿者</p>
								<div class="name">
									<span class="name"><c:out value="${comment.name}" /></span><br/>
								</div>
								<p class="label">コメント</p>
								<div class="text">
									<span class="text"><c:forEach var="text" items="${fn:split(comment.text,'
')}" ><c:out value="${text}" /><br></c:forEach></span><br/>
								</div>

								<div class="date">
									<span class="createdDate"><c:out value="${comment.createdDate}" /></span><br/>
								</div>

								<c:if test="${ loginUser.id==comment.userId }">
									<form action="deleteComment" method="post" onSubmit="return check()">
										<div class="deleteButton">
											<input type="hidden" name="commentId" value="${comment.commentId}">
											<input type="submit" value="削除">
										</div>
									</form>
								</c:if>

							</c:if>
						</c:forEach>

					</div>

					<div class="comment">
						<form action="comment" method="post">
							<input type="hidden" name="messageId" id="messageId" value="${message.messageId}">
							<textarea name="comment" cols="35" rows="5" id="comment"><c:out value="${comment.text}" /></textarea><br />

							<input type="submit" name="comment" value="コメント投稿">
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)Yamazaki Masaya</div>
	</div>
</body>
</html>