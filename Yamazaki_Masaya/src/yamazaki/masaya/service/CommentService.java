package yamazaki.masaya.service;

import static yamazaki.masaya.utils.CloseableUtil.*;
import static yamazaki.masaya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.Comment;
import yamazaki.masaya.beans.UserComment;
import yamazaki.masaya.dao.CommentDao;
import yamazaki.masaya.dao.UserCommentDao;

public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}


	public List<UserComment> getComments() {

		List<UserComment> comment = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao userCommentDao = new UserCommentDao();
			comment = userCommentDao.getUserComments(connection);

			commit(connection);

			return comment;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
//
//	public List<UserMessages> getMessage(int userId) {
//
//		List<UserMessages> messages = new ArrayList<>();
//		Connection connection = null;
//		try {
//			connection = getConnection();
//
//			UserMessageDao userMessageDao = new UserMessageDao();
//			messages = userMessageDao.getUserMessage(connection, userId);
//
//			commit(connection);
//
//			return messages;
//
//		} catch (RuntimeException e) {
//			rollback(connection);
//			throw e;
//
//		} catch (Error e) {
//			rollback(connection);
//			throw e;
//
//		} finally {
//			close(connection);
//		}
//}

	    public void delete(int commentId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            CommentDao commentDao = new CommentDao();
	            commentDao.delete(connection, commentId);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;

	        } catch (Error e) {
	            rollback(connection);
	            throw e;

	        } finally {
	            close(connection);
	        }
	    }

}