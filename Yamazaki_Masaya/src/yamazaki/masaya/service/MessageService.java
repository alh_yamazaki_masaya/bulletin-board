package yamazaki.masaya.service;

import static yamazaki.masaya.utils.CloseableUtil.*;
import static yamazaki.masaya.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import yamazaki.masaya.beans.Messages;
import yamazaki.masaya.beans.UserMessages;
import yamazaki.masaya.dao.MessageDao;
import yamazaki.masaya.dao.UserMessageDao;

public class MessageService {

	public void register(Messages message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);



		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserMessages> getMessages(String dateFrom, String dateTo, String category) {

		List<UserMessages> messages = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();

			if (dateFrom == null || dateFrom.isEmpty()) {
				dateFrom = "2020-01-01 00:00:00";
			} else {
				dateFrom = dateFrom + " 00:00:00";
			}

			if (dateTo == null || dateTo.isEmpty()) {
				Date date = new Date();
				SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				dateTo = sDateFormat.format(date);
			} else {
				dateTo = dateTo + " 23:59:59";
			}

			if (category == null || category.isEmpty()) {
				category = "";
			}

			UserMessageDao userMessageDao = new UserMessageDao();
			messages = userMessageDao.getUserMessages(connection, dateFrom, dateTo, category);

			commit(connection);

			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, messageId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}