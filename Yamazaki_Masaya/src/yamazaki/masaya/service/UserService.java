package yamazaki.masaya.service;

import static yamazaki.masaya.utils.CloseableUtil.*;
import static yamazaki.masaya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import yamazaki.masaya.beans.User;
import yamazaki.masaya.beans.UserBranchDepartment;
import yamazaki.masaya.dao.UserBranchDepartmentDao;
import yamazaki.masaya.dao.UserDao;
import yamazaki.masaya.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public User login(String account, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUsers(connection, account, encPassword);

			commit(connection);

			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public User getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, userId);

			commit(connection);

			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			if (StringUtils.isNotBlank(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public void updateIsStopped(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.updateIsStopped(connection, user);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserBranchDepartment> getUserBranchDepartments() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserBranchDepartmentDao userBranchDepartmentDao = new UserBranchDepartmentDao();
			List<UserBranchDepartment> userBranchDepartmentList = userBranchDepartmentDao
					.getUserBranchDepartments(connection);

			commit(connection);

			return userBranchDepartmentList;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public boolean isRegisteredUser(String account) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			int userCount = userDao.getUserCount(connection, account);

			commit(connection);

			if (userCount < 1) {
				return false;
			}
			return true;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

}