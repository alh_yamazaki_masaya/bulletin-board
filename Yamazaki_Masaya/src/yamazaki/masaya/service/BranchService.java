package yamazaki.masaya.service;

import static yamazaki.masaya.utils.CloseableUtil.*;
import static yamazaki.masaya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.Branch;
import yamazaki.masaya.beans.Comment;
import yamazaki.masaya.dao.BranchDao;
import yamazaki.masaya.dao.CommentDao;

public class BranchService {

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}


	public List<Branch> getBranches() {

		List<Branch> branches = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			branches = branchDao.getBranches(connection);

			commit(connection);

			return branches;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
//
//	public List<UserMessages> getMessage(int userId) {
//
//		List<UserMessages> messages = new ArrayList<>();
//		Connection connection = null;
//		try {
//			connection = getConnection();
//
//			UserMessageDao userMessageDao = new UserMessageDao();
//			messages = userMessageDao.getUserMessage(connection, userId);
//
//			commit(connection);
//
//			return messages;
//
//		} catch (RuntimeException e) {
//			rollback(connection);
//			throw e;
//
//		} catch (Error e) {
//			rollback(connection);
//			throw e;
//
//		} finally {
//			close(connection);
//		}
//}

	    public void delete(int commentId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            CommentDao commentDao = new CommentDao();
	            commentDao.delete(connection, commentId);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;

	        } catch (Error e) {
	            rollback(connection);
	            throw e;

	        } finally {
	            close(connection);
	        }
	    }

}