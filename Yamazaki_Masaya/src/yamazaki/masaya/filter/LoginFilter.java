package yamazaki.masaya.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import yamazaki.masaya.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;

		List<String> errorMessages = new ArrayList<>();

		User user = (User) session.getAttribute("loginUser");

		if(!isLoginAccess(httpReq)) {

			if (user == null) {

				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);
				// メッセージを格納して、リダイレクト
				httpRes.sendRedirect("login");
				return;
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

	private boolean isLoginAccess(HttpServletRequest request) {

		String servletPath = request.getServletPath();
		if (servletPath.matches(".*login.*")) {

			return true;
		}else if(servletPath.endsWith("css")) {
			return true;
		}
		return false;

	}

}
