package yamazaki.masaya.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import yamazaki.masaya.beans.User;

@WebFilter("/*")
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;

		List<String> errorMessages = new ArrayList<>();

		User user = (User) session.getAttribute("loginUser");

		if (!isAuthorityAccount(httpReq, user)) {

			errorMessages.add("アクセス権限がありません");

			httpReq.setAttribute("errorMessages", errorMessages);
			httpReq.getRequestDispatcher("./").forward(request, response);
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}


	private boolean isAuthorityAccount(HttpServletRequest request, User user) {

		String servletPath = request.getServletPath();

		if (servletPath.matches(".*manegement.*") ||
				servletPath.matches(".*signup.*") ||
				servletPath.matches(".*setting.*")) {

			int branchId = user.getBranchId();
			int departmentId = user.getDepartmentId();

			if (branchId == 1 && departmentId == 1) {
				return true;
			}

			// 権限の必要なページにアクセスし、権限がない場合
			return false;
		}
		return true;
	}

}
