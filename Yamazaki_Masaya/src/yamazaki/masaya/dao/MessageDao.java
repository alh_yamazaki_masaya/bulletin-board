package yamazaki.masaya.dao;

import static yamazaki.masaya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.Messages;
import yamazaki.masaya.beans.User;
import yamazaki.masaya.exception.NoRowsDeletedRuntimeException;
import yamazaki.masaya.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Messages message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//    public User getUser(Connection connection, int id) {
	//
	//        PreparedStatement ps = null;
	//        try {
	//            String sql = "SELECT * FROM users WHERE id = ?";
	//
	//            ps = connection.prepareStatement(sql);
	//            ps.setInt(1, id);
	//
	//            ResultSet rs = ps.executeQuery();
	//            List<User> userList = toUserList(rs);
	//
	//            if (userList.isEmpty()) {
	//                return null;
	//
	//            } else if (2 <= userList.size()) {
	//                throw new IllegalStateException("2 <= userList.size()");
	//
	//            } else {
	//                return userList.get(0);
	//            }
	//        } catch (SQLException e) {
	//            throw new SQLRuntimeException(e);
	//
	//        } finally {
	//            close(ps);
	//        }
	//    }
	//
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = Integer.parseInt(rs.getString("branch_id"));
				int departmentId = Integer.parseInt(rs.getString("department_id"));
				int isStopped = Integer.parseInt(rs.getString("is_stopped"));
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();

				user.setId(id);
				user.setAccount(account);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setIsStopped(isStopped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;

		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages ");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsDeletedRuntimeException();
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}

	}

}