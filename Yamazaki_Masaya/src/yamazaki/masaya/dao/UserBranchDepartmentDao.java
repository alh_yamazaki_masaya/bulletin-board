package yamazaki.masaya.dao;

import static yamazaki.masaya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.UserBranchDepartment;
import yamazaki.masaya.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> getUserBranchDepartments(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user.id as userId, ");
			sql.append("user.account as account, ");
			sql.append("user.name as userName, ");
			sql.append("user.is_stopped as isStopped, ");
			sql.append("branch.name as branchName, ");
			sql.append("department.name as departmentName ");
			sql.append("FROM users user ");
			sql.append("JOIN branches branch ");
			sql.append("ON user.branch_id = branch.id ");
			sql.append("JOIN departments department ");
			sql.append("ON user.department_id = department.id ");


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartmentList = toUserBranchDepartmentList(rs);

			if (userBranchDepartmentList == null) {
				return null;

			}  else {
				return userBranchDepartmentList;
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//    public User getUser(Connection connection, int id) {
	//
	//        PreparedStatement ps = null;
	//        try {
	//            String sql = "SELECT * FROM users WHERE id = ?";
	//
	//            ps = connection.prepareStatement(sql);
	//            ps.setInt(1, id);
	//
	//            ResultSet rs = ps.executeQuery();
	//            List<User> userList = toUserList(rs);
	//
	//            if (userList.isEmpty()) {
	//                return null;
	//
	//            } else if (2 <= userList.size()) {
	//                throw new IllegalStateException("2 <= userList.size()");
	//
	//            } else {
	//                return userList.get(0);
	//            }
	//        } catch (SQLException e) {
	//            throw new SQLRuntimeException(e);
	//
	//        } finally {
	//            close(ps);
	//        }
	//    }
	//
	private List<UserBranchDepartment> toUserBranchDepartmentList(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> ret = new ArrayList<>();
		try {
			while (rs.next()) {

				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				int userId = rs.getInt("userId");
				String account = rs.getString("account");
				String userName = rs.getString("userName");
				int isStopped = Integer.parseInt(rs.getString("isStopped"));
				String branchName = rs.getString("branchName");
				String departmentName = rs.getString("departmentName");

				userBranchDepartment.setUserId(userId);
				userBranchDepartment.setAccount(account);
				userBranchDepartment.setUserName(userName);
				userBranchDepartment.setIsStopped(isStopped);
				userBranchDepartment.setBranchName(branchName);
				userBranchDepartment.setDepartmentName(departmentName);


				ret.add(userBranchDepartment);
			}
			return ret;

		} finally {
			close(rs);
		}
	}
	//
	//    public void update(Connection connection, User user) {
	//
	//        PreparedStatement ps = null;
	//        try {
	//            StringBuilder sql = new StringBuilder();
	//            sql.append("UPDATE users SET");
	//            sql.append("  account = ?");
	//            sql.append(", name = ?");
	//            sql.append(", email = ?");
	//            sql.append(", password = ?");
	//            sql.append(", description = ?");
	//            sql.append(", updated_date = CURRENT_TIMESTAMP");
	//            sql.append(" WHERE");
	//            sql.append(" id = ?");
	//
	//            ps = connection.prepareStatement(sql.toString());
	//
	//            ps.setString(1, user.getAccount());
	//            ps.setString(2, user.getName());
	//            ps.setString(3, user.getEmail());
	//            ps.setString(4, user.getPassword());
	//            ps.setString(5, user.getDescription());
	//            ps.setInt(6, user.getId());
	//
	//            int count = ps.executeUpdate();
	//
	//            if (count == 0) {
	//                throw new NoRowsUpdatedRuntimeException();
	//            }
	//
	//        } catch (SQLException e) {
	//            throw new SQLRuntimeException(e);
	//
	//        } finally {
	//            close(ps);
	//        }
	//
	//    }
	//

}