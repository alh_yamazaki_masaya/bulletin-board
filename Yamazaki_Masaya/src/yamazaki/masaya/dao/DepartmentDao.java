package yamazaki.masaya.dao;

import static yamazaki.masaya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.Department;
import yamazaki.masaya.exception.SQLRuntimeException;

public class DepartmentDao {

	//	public void insert(Connection connection, Comment comment) {
	//
	//		PreparedStatement ps = null;
	//		try {
	//			StringBuilder sql = new StringBuilder();
	//			sql.append("INSERT INTO comments ( ");
	//			sql.append("text");
	//			sql.append(", user_id");
	//			sql.append(", message_id");
	//			sql.append(", created_date");
	//			sql.append(", updated_date");
	//			sql.append(") VALUES (");
	//			sql.append("?"); // text
	//			sql.append(", ?"); // user_id
	//			sql.append(", ?"); // message_id
	//			sql.append(", CURRENT_TIMESTAMP"); // created_date
	//			sql.append(", CURRENT_TIMESTAMP"); // updated_date
	//			sql.append(")");
	//
	//			ps = connection.prepareStatement(sql.toString());
	//
	//			ps.setString(1, comment.getText());
	//			ps.setString(2, String.valueOf(comment.getUserId()));
	//			ps.setString(3,String.valueOf(comment.getMessageId()));
	//
	//			ps.executeUpdate();
	//
	//		} catch (SQLException e) {
	//			throw new SQLRuntimeException(e);
	//
	//		} finally {
	//			close(ps);
	//		}
	//	}
	//

	public List<Department> getDepartments(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Department> departmentList = toDepartmentList(rs);

			if (departmentList.isEmpty()) {
				return null;

			} else {
				return departmentList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				Department department = new Department();

				department.setId(id);
				department.setName(name);
				department.setCreatedDate(createdDate);
				department.setUpdatedDate(updatedDate);

				ret.add(department);
			}
			return ret;

		} finally {
			close(rs);
		}
	}

	//	public void delete(Connection connection, int commentId) {
	//
	//		PreparedStatement ps = null;
	//		try {
	//			StringBuilder sql = new StringBuilder();
	//			sql.append("DELETE FROM comments ");
	//			sql.append(" WHERE");
	//			sql.append(" id = ?");
	//
	//			ps = connection.prepareStatement(sql.toString());
	//
	//			ps.setInt(1, commentId);
	//
	//			int count = ps.executeUpdate();
	//
	//			if (count == 0) {
	//				throw new NoRowsDeletedRuntimeException();
	//			}
	//
	//		} catch (SQLException e) {
	//			throw new SQLRuntimeException(e);
	//
	//		} finally {
	//			close(ps);
	//		}
	//
	//	}

}