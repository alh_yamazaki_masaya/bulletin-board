package yamazaki.masaya.dao;

import static yamazaki.masaya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.UserComment;
import yamazaki.masaya.exception.SQLRuntimeException;

public class UserCommentDao {


	public List<UserComment> getUserComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user.id as id, ");
			sql.append("user.name as name, ");
			sql.append("comment.id as comment_id, ");
			sql.append("comment.text as text, ");
			sql.append("comment.message_id as message_id, ");
			sql.append("comment.created_date as created_date ");
			sql.append("FROM comments comment ");
			sql.append("INNER JOIN users user ");
			sql.append("ON comment.user_id = user.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> userCommentList = toUserMessageList(rs);

			return userCommentList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {

				int userId = rs.getInt("id");
				String name = rs.getString("name");
				int commentId = rs.getInt("comment_id");
				String text = rs.getString("text");
				int messageId = rs.getInt("message_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setUserId(userId);
				comment.setName(name);
				comment.setCommentId(commentId);
				comment.setText(text);
				comment.setMessageId(messageId);
				comment.setCreatedDate(createdDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}