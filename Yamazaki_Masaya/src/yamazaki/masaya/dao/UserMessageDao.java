package yamazaki.masaya.dao;

import static yamazaki.masaya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yamazaki.masaya.beans.UserMessages;
import yamazaki.masaya.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessages> getUserMessages(Connection connection, String dateFrom, String dateTo,
			String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user.id as userId, ");
			sql.append("user.name as name, ");
			sql.append("msg.id as messageId, ");
			sql.append("msg.title as title, ");
			sql.append("msg.category as category, ");
			sql.append("msg.text as text, ");
			sql.append("msg.created_date as created_date ");
			sql.append("FROM messages msg ");
			sql.append("INNER JOIN users user ");
			sql.append("ON msg.user_id = user.id ");
			sql.append("WHERE msg.category LIKE ? ");
			sql.append("AND msg.created_date between ? and ? ");
			sql.append("ORDER BY msg.created_date DESC;");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, "%"+category+"%");
			ps.setString(2, dateFrom);
			ps.setString(3, dateTo);

			ResultSet rs = ps.executeQuery();
			List<UserMessages> userMessageList = toUserMessageList(rs);

			return userMessageList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	private List<UserMessages> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessages> ret = new ArrayList<UserMessages>();
		try {
			while (rs.next()) {

				int userId = rs.getInt("userId");
				String name = rs.getString("name");
				int messageId = rs.getInt("messageId");
				String title =  rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessages message = new UserMessages();
				message.setUserId(userId);
				message.setName(name);
				message.setMessageId(messageId);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}