package yamazaki.masaya.beans;

import java.io.Serializable;

public class UserBranchDepartment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int userId;
	private String account;
	private String userName;
	private String branchName;
	private String departmentName;
	private int isStopped;

	public int getUserId() {
		return userId;
	}

	public String getAccount() {
		return account;
	}

	public String getuserName() {
		return userName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public int getIsStopped() {
		return isStopped;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}


}
