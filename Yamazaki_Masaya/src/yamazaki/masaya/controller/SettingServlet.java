package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import yamazaki.masaya.beans.Branch;
import yamazaki.masaya.beans.Department;
import yamazaki.masaya.beans.User;
import yamazaki.masaya.beans.UserBranchDepartment;
import yamazaki.masaya.service.BranchService;
import yamazaki.masaya.service.DepartmentService;
import yamazaki.masaya.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		UserService userService = new UserService();
		User user = new User();

		try {
			int userId = Integer.parseInt(request.getParameter("userId"));

			user = userService.getUser(userId);

			if (user == null) {
				throw new Exception();
			}

			List<Branch> branches = new BranchService().getBranches();
			List<Department> departments = new DepartmentService().getDepartments();

			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("setting.jsp").forward(request, response);

		} catch (Exception e) {

			List<UserBranchDepartment> userBranchDepartments = userService.getUserBranchDepartments();

			List<String> messages = new ArrayList<>();

			messages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", messages);
			request.setAttribute("userBrancbDepartments", userBranchDepartments);
			request.getRequestDispatcher("/manegement.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		UserService userService = new UserService();
		User user = userService.getUser(Integer.parseInt(request.getParameter("userId")));

		if (isValid(request, messages, user)) {

			setUser(request, user);

			new UserService().update(user);

			response.sendRedirect("manegement");
		} else {

			setUser(request, user);

			List<Branch> branches = new BranchService().getBranches();
			List<Department> departments = new DepartmentService().getDepartments();

			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User user) {

		String beforAccount = user.getAccount();
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		if (StringUtils.isEmpty(account)) {
			messages.add("アカウント名を入力してください");

		} else if (!account.matches("^[a-zA-Z0-9]{6,20}$")) {
			messages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
		}
		if (!beforAccount.equals(account)) {
			if (new UserService().isRegisteredUser(account)) {
				messages.add("そのアカウント名は既に利用されています");
			}
		}

		if (!StringUtils.isEmpty(password)) {

			if (!password.matches("^[-_@\\.a-zA-Z0-9]{6,20}$")) {
				messages.add("パスワードは半角英数字記号で6文字以上20文字以下で入力してください");
			}
			if (StringUtils.isEmpty(confirmationPassword)) {
				messages.add("確認用パスワードを入力してください");
			} else if (!(password.equals(confirmationPassword))) {
				messages.add("パスワードと確認用パスワードが違います");
			}

		}
		if (StringUtils.isBlank(name)) {
			messages.add("ユーザー名を入力してください");
		} else if (name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

		if (branchId == 1) {

			// enamを使うといいかも
			// 3は営業部,4は技術部
			if (departmentId == 3 || departmentId == 4) {
				messages.add("本社勤務では選択した部署は選べません");
			}
		} else {

			// enamを使うといいかも
			// 1は総務人事部,2は情報部
			if (departmentId == 1 || departmentId == 2) {
				messages.add("本社勤務以外で選択した部署は選べません");
			}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private void setUser(HttpServletRequest request, User user) {

		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

	}
}