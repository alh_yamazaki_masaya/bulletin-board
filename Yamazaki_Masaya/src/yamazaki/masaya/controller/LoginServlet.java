package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yamazaki.masaya.beans.User;
import yamazaki.masaya.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String accountId = request.getParameter("accountId");
		String password = request.getParameter("password");

		UserService userService = new UserService();
		User user = userService.login(accountId, password);

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (!isValid(request, messages)) {

			request.setAttribute("errorMessages", messages);
			request.setAttribute("id", accountId);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			if (user == null) {

				messages.add("IDまたはパスワードが間違っています。");
				request.setAttribute("errorMessages", messages);
				request.setAttribute("id", accountId);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}else if (user.getIsStopped() == 1) {

				messages.add("現在このアカウントは使用できません");
				request.setAttribute("errorMessages", messages);
				request.setAttribute("id", accountId);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}

			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("accountId");
		String password = request.getParameter("password");

		if (StringUtils.isEmpty(account)) {
			messages.add("アカウント名を入力してください");

		}
		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");

		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}