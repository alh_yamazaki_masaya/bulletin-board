package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yamazaki.masaya.beans.Comment;
import yamazaki.masaya.beans.User;
import yamazaki.masaya.beans.UserComment;
import yamazaki.masaya.beans.UserMessages;
import yamazaki.masaya.service.CommentService;
import yamazaki.masaya.service.MessageService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

    	HttpSession session = request.getSession();

		if (isValid(request, messages)) {

			User user = (User) session.getAttribute("loginUser");
			Comment comment = new Comment();

			comment.setText(request.getParameter("comment"));
			comment.setUserId(user.getId());
			comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));


			new CommentService().register(comment);


			//TODO 登録完了メッセージの追加？

	        String dateFrom = request.getParameter("dateFrom");
	        String dateTo = request.getParameter("dateTo");
	        String category = request.getParameter("category");

	        List<UserMessages> userMessages = new MessageService().getMessages(dateFrom,dateTo,category);

			List<UserComment>userComments = new CommentService().getComments();


			request.setAttribute("messages", userMessages);
			request.setAttribute("comments",userComments);

			request.getRequestDispatcher("./home.jsp").forward(request, response);
		} else {
			request.setAttribute("errorMessages", messages);


	        String dateFrom = request.getParameter("dateFrom");
	        String dateTo = request.getParameter("dateTo");
	        String category = request.getParameter("category");

	        List<UserMessages> userMessages = new MessageService().getMessages(dateFrom,dateTo,category);

			List<UserComment>userComments = new CommentService().getComments();


			request.setAttribute("messages", userMessages);
			request.setAttribute("comments",userComments);

			request.getRequestDispatcher("./home.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		//TODO エラーの実装はまた今度
		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment)) {
			messages.add("コメントを入力してください");
		}
		if(comment.length() > 500) {
			messages.add("コメントは500文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}



}