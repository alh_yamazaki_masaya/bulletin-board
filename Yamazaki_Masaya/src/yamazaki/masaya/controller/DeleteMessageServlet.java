package yamazaki.masaya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yamazaki.masaya.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//    @Override
	//    protected void doGet(HttpServletRequest request,
	//            HttpServletResponse response) throws IOException, ServletException {
	//
	//        request.getRequestDispatcher("login.jsp").forward(request, response);
	//    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		int messageId = Integer.parseInt(request.getParameter("messageId"));
		new MessageService().delete(messageId);

		response.sendRedirect("./");
	}
}