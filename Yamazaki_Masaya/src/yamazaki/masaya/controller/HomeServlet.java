package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yamazaki.masaya.beans.UserComment;
import yamazaki.masaya.beans.UserMessages;
import yamazaki.masaya.service.CommentService;
import yamazaki.masaya.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        String category = request.getParameter("category");

        List<UserMessages> userMessages = new MessageService().getMessages(dateFrom,dateTo,category);


		List<UserComment>userComments = new CommentService().getComments();



		request.setAttribute("messages", userMessages);
		request.setAttribute("comments",userComments);

		request.setAttribute("dateFrom", dateFrom);
		request.setAttribute("dateTo", dateTo);
		request.setAttribute("category", category);


		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

}