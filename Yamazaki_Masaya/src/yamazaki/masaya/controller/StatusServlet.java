package yamazaki.masaya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import yamazaki.masaya.beans.User;
import yamazaki.masaya.service.UserService;

@WebServlet(urlPatterns = { "/status" })
public class StatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("userId")));

		if (request.getParameter("stop") != null) {
			user.setIsStopped(1);
		} else if (request.getParameter("resurrect") != null) {
			user.setIsStopped(0);
		}

		new UserService().updateIsStopped(user);

		response.sendRedirect("manegement");

	}

}
