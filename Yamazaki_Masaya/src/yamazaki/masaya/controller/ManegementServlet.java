package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yamazaki.masaya.beans.UserBranchDepartment;
import yamazaki.masaya.service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		UserService userService = new UserService();
		List<UserBranchDepartment> userBranchDepartments = userService.getUserBranchDepartments();

		if (userBranchDepartments != null) {

			request.setAttribute("userBrancbDepartments", userBranchDepartments);
			request.getRequestDispatcher("/manegement.jsp").forward(request, response);

		} else {
			request.getRequestDispatcher("/manegement.jsp").forward(request, response);
		}
	}

}