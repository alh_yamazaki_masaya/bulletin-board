package yamazaki.masaya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yamazaki.masaya.beans.Messages;
import yamazaki.masaya.beans.User;
import yamazaki.masaya.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		if (isValid(request, errorMessages)) {

			User user = (User) session.getAttribute("loginUser");
			Messages message = new Messages();

			message.setUserId(user.getId());
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("text"));

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {

			Messages message = new Messages();
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("text"));

			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");
		}
		if (title.length() > 30) {
			errorMessages.add("件名は30文字以下で入力してください");
		}
		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリを入力してください");
		}
		if (category.length() > 10) {
			errorMessages.add("カテゴリは10文字以下で入力してください");
		}
		if (StringUtils.isBlank(text)) {
			errorMessages.add("投稿内容を入力してください");
		}
		if (text.length() > 1000) {
			errorMessages.add("投稿内容は1000文字以下で入力してください");
		}
		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}